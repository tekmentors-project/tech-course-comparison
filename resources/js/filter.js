var filterCourses = (function(){
    
    var value;
    var  matchSel;
    var filterChoice = {};

   function initializeFilter(){
    $("#courseList .card").hide();
    $('.filter-section :checkbox').each((i,item) => {
        var currentId =  $(item)[0].id;
        filterChoice[currentId] = '';
      });
   }

   function captureFilterSelected(){
        //var filterChoice = [{type1:'',type2:'',medium1:'',medium2:'',level1:'',level2:''}];
        $(".filter-section :checkbox:checked").each((index,item) => {
            value =  $(item).val();
            matchSel = $(item)[0].id;
            filterChoice[matchSel] = value;
        });
   }

   function applyFilterLogic(){
    $('.card').each((i,card) => {
        let isType =  (filterChoice.type1 == '' && filterChoice.type2 == '');
        let isMedium =  (filterChoice.medium1 == '' && filterChoice.medium2 == '');
        let isLevel = (filterChoice.level1 == '' && filterChoice.level2 == '');
        var type = (isType || $(card).attr('data-type') == filterChoice.type1 || $(card).attr('data-type') == filterChoice.type2);
        var medium = (isMedium || $(card).attr('data-medium') == filterChoice.medium1 || $(card).attr('data-medium') == filterChoice.medium2);
        var level = (isLevel || $(card).attr('data-level') == filterChoice.level1 || $(card).attr('data-level') == filterChoice.level2);
        var isShow = (type && medium && level) ;
        isShow ? $(card).fadeIn() : ''; 
       });
       if($('.filter-section :checkbox').filter(':checked').length < 1) {
        $("#courseList .card").fadeIn();
      }
      if($('.card:visible').length == 0){
          $('#no-course-msg').removeClass('d-none');
      } else {
        $('#no-course-msg').addClass('d-none');
      }
   }


   return {
       filter : function(){
        $(".filter-section :checkbox").click(() => {
           initializeFilter();
           captureFilterSelected();
           applyFilterLogic();
        });
       }
   }
})();

