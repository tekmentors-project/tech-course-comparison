// Initialize Firebase
        // TODO: Replace with your project's customized code snippet
 var firebaseMethods = (function(){
        var config = {
            apiKey: "AIzaSyDuDJTmUOqFfXz8Q-1wcH3vR20hSQaPJhY",
            authDomain: "ilearn360-6ca2b.firebaseapp.com",
            databaseURL: "https://ilearn360-6ca2b.firebaseio.com/",
            projectId: "ilearn360-6ca2b"
           
          };
          firebase.initializeApp(config);
      let rootref = firebase.database().ref();
      let query = '';
      return {
        addNewCourseToFirebase: function (db_Child, postData,courseAdditinalInfo,courseComments) {
                // Get a key for a new Course.
            var newPostKey = rootref.child(db_Child).push().key;
            // Write the new course's data in the course list .
            var updates = {};
            updates['/'+db_Child+'/' + newPostKey] = postData;
            updates['/course_details'+'/' + newPostKey] = courseAdditinalInfo;
            updates['/course_comments/' + newPostKey] = courseComments;
            return firebase.database().ref().update(updates);
       },
       fetchCategoriesData: function() {
            query = firebase.database().ref("categories").orderByKey();
            return query.once("value").then(function(snapshot) {
              return snapshot;
           });
         },
        fetchSelectedCategoryCourses: function(category) {
            query = rootref.child("courses_list").orderByChild('category_id').equalTo(category);
            return query.once("value").then(function(snapshot) {
              return snapshot;
          });
        },
        addVoteToCourse: function(course_id,upvote_count,isVoted){
          query = rootref.child("courses_list/"+course_id);
          query.update({upvote :  upvote_count});
          query.update({isVoted :  isVoted});
        }, 
        fetchCourseDetail: function(queryParam,courseSelected){
          query = rootref.child(queryParam).child(courseSelected); 
           return query.once("value").then(function(snapshot) {
           return snapshot.val();
        });
        },
        updateUserComments: function(key,comment,course_id){
          var updates = {};
          updates[key] = comment;
          firebase.database().ref("course_comments/"+ course_id +  '/' + 'comments').update(updates);
        },
        updateVoteToCourse: function(course_id,param_name,param_value){
          query = rootref.child("course_details/"+course_id);
          query.update({param_name : param_value});
          var updates = {};
          updates["course_details/"+course_id + '/' + param_name] = param_value;
          firebase.database().ref().update(updates);
        } 
      }
  })();



  