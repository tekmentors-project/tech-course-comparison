var sortMethods = (function(){
    return {
        getVoteSorted : function(selector, attrName) {
        return $(
            $(selector).toArray().sort(function(a, b){
                var aVal = parseInt(a.getAttribute(attrName)),
                    bVal = parseInt(b.getAttribute(attrName));
                    if($('#sort #vote i.fa').hasClass('fa-arrow-up')){
                       return  bVal - aVal;
                    } else {
                        return aVal - bVal;
                    }
            })
        );
    },
        getAuthorSorted : function(selector, attrName) {
          return $( 
                $(selector).sort(function (a, b) {
                 var at = a.getAttribute(attrName);
                 var bt = b.getAttribute(attrName);
                 if($('#sort #submit i.fa').hasClass('fa-arrow-up')){
                    return (at < bt) ? 1 : 0;
                } else {
                    return (at > bt) ? 1 : 0;
                }
                })
            );
        },
        toggleSortArrow : function(selector){
            $('#sort #'+ selector).addClass('text-success font-weight-bold');
            if($('#sort #'+ selector + ' i.fa').hasClass('fa-arrow-up')){
                $('#sort #'+ selector + ' i.fa').addClass('fa-arrow-down').removeClass('fa-arrow-up');
                }else{
                $('#sort #'+ selector + ' i.fa').removeClass('fa-arrow-down').addClass('fa-arrow-up');
            } 
        }
    }
})();