var model = (function(){
var TEMPLATES = {}
TEMPLATES = {
    index: {
        name:'index',
        html: `resources/templates/index.html`
    },
    courses: {
        name:'courses',
        html: `resources/templates/courses.html`
    },
    courseDetails:{
        name:'course-details',
        html: `resources/templates/course-details.html`
    }
   
}

let courseDetails = {
    course_url: '',
    category_id:'',
    level:'',
    medium:'',
    submittedBy:'',
    title:'',
    type:'',
    upvote:'0',
    isVoted:'false'
}

let courseAdditinalInfo = {
    author : "",
    category_id : "",
    course_pace : 0,
    course_url : "",
    data_field : 0,
    depth : 0,
    details : "This is the description",
    instructor : 0,
    is_course_pace_voted : "false",
    is_data_field_voted : "false",
    is_depth_voted : "false",
    is_instructor_voted : "false",
    is_quality_voted : "false",
    is_quality_votes_voted : "false",
    param_name : "false",
    quality : 0,
    quality_votes : 0
}

let courseComments = {
    comments: []
}

return {
    courseDetails: courseDetails,
    courseAdditinalInfo: courseAdditinalInfo,
    courseComments: courseComments
}

})();
