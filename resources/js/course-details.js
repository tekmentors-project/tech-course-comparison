    $(document).ready(function(){
      //  courseDetail.init();
    });

    var courseDetail = (function(){ 

    var rootref = firebase.database().ref();
    var course_id = "";
    var commentsLength = 0;
    function init(catogery_id){
        course_id = catogery_id;
        initializeCourseDetails();
        fetchCourseDataInParallel()
        }
    
     function initializeCourseDetails(){
        var queryString = new Array();
        if (queryString.length == 0) {
            if (window.location.search.split('?').length > 1) {
                var params = window.location.search.split('?')[1].split('&');
                for (var i = 0; i < params.length; i++) {
                    var key = params[i].split('=')[0];
                    var value = decodeURIComponent(params[i].split('=')[1]);
                    queryString[key] = value;
                }
            }
        }
            let courseSelected = queryString[key];
            if(courseSelected){
            course_id = courseSelected;
        
            }
     }

     function fetchCourseDataInParallel(){
        $('#spinner').addClass('d-flex mx-auto').removeClass('d-none');
            let arr = [];
            arr.push(fetchCourseBasicInfo(course_id));
            arr.push(fetchCourseDetail(course_id));
            arr.push(fetchCourseComments(course_id));
            return Promise.all(arr).then((results) => {
                return populateCoursesDetails(results[0], results[1],results[2]);
            });
     }
 
    function fetchCourseDetail(courseSelected){
        return firebaseMethods.fetchCourseDetail("course_details",courseSelected);
    }

    function fetchCourseBasicInfo(courseSelected){
        return firebaseMethods.fetchCourseDetail("courses_list",courseSelected);
    }

    function fetchCourseComments(courseSelected){
        return firebaseMethods.fetchCourseDetail("course_comments",courseSelected);
    }

    function populateCoursesDetails(courseList, courseDetails,courseComments){
        let hostName = common.queryCourseDomain(courseList.course_url);
        updateCourseHeader(courseList,courseDetails,hostName);
        updateCourseAsideSection(courseList,courseDetails);
        visitTutorialSection(courseList);
        initializeVoteEventListener();
        var commentSection = populateCourseComments(courseComments);
        $('#commentSection').append(commentSection);
        oneLinerReviewVote();
        $('#spinner').removeClass('d-flex mx-auto').addClass('d-none');
    }

    function oneLinerReviewVote(){
        $('.course_desc .tut-vote a.desc').click((e) => {
            var data_field = e.target.closest('a').id;
            $(e.target.closest('a')).toggleClass('text-dark text-success');
            var courseid =  course_id;
            var vote_count =  parseInt($(e.target.closest('a')).find('.count').text(),10);
            $(e.target.closest('a')).hasClass('text-success') ? vote_count += 1 : vote_count -= 1;
            $(e.target.closest('a')).find('.count').text(vote_count);
            firebaseMethods.updateVoteToCourse(course_id,data_field,vote_count);
            let isVotedId = $(e.target.closest('.tut-vote .arrow'))[0].id;
            var isVoted = $(e.target.closest('a')).hasClass('text-success') ? "true" : "false";
            firebaseMethods.updateVoteToCourse(course_id,isVotedId,isVoted); 
         });
    }


    function visitTutorialSection(courseList){
        const navigateCourse = `<a href=${courseList.course_url}  class="btn btn-primary my-3 w-100">Visit Tutorial</a>`;
        $('.filter-section div').append(navigateCourse); 
    }

    function updateCourseAsideSection(courseList,courseDetails){
        const asideTmpl = ` <div class="panel-heading border-bottom py-3">
        <h5>Tutorial Info</h5>
    </div>
   <div class="filter-section">
       <div class="mt-3">
          <label class="w-50"><strong>Session:</strong> </label>
          <span class="">${courseList.type}-${courseList.medium}</span> <br/>
          <label class="w-50"><strong>Medium:</strong></label>
          <span class="">${courseList.medium}</span> <br/>
          <label class="w-50"><strong>Certificate:</strong> </label>
          <span class="">On Completion</span> <br/>
          <label class="w-50"><strong>Taught by: </strong></label>
          <span class="">${courseDetails.author}</span> <br/>
          <label class="w-50"><strong>Submitted by: </strong></label>
          <span class="">${courseList.submittedBy}</span> <br/>
          
       </div>
   </div>`;
   $('#course-aside').append(asideTmpl);
    }

    function updateCourseHeader(courseList,courseDetails,hostName){
        const courseHeader = `
        <div class="accordion" id="courseList">
        <div id="spinner" class="lds-spinner d-none"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>

          <div class="card">
          <div class="card-header d-flex" id="headingOne">
                <div class="tut-vote mr-3 align-self-center">
                <a href="javascript:void(0)" class="vote-widget js-tutorial-upvote ${courseList.isVoted == 'true' ? 'bg-success text-white' : ''}" data-message=" " data-topic="JavaScript" data-url=${courseList.course_url} data-id="3768"> 
                            <span class="arrow">
                                <i class="fa fa-caret-up"></i>
                            </span>
                            <span class="count">${courseList.upvote}</span>
                        </a>
                 </div>
                <div class="tut-title">
                    <div class="title-links mb-2">
                        <a href=${courseList.course_url} class="js-tutorial-title">
                        <span class="tutorial-title-txt">${courseList.title}</span>
                        <span class="tut-title-link">
                            <small class="js-tutorial text-dark">(${hostName})</small>
                        </span>
                     </a>
                </div>
                <div class="tut-footer">
                        <a href=${courseList.course_url} class="btn btn-outline-primary px-3 mr-5" >Visit Tutorial</a>
                        <span class="badge badge-info">${courseList.type}</span>
                        <span class="badge  badge-info">${courseList.medium}</span>
                        <span class="badge badge-info">${courseList.level}</span>
                </div>
            </div>
          

          </div>
      
       <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                    <div class="section-title text-left">
                     <p class="sec--title">Why developers like this tutorial(one liner review):</p>
                    </div>   
                    <div class="course_desc d-flex flex-wrap">
                    <div class="one-liner w-25 mr-3">
                            <div class="tut-vote info d-flex">
                                <a href="javascript:void(0);" id="quality" class="d-flex desc flex-column ${courseDetails.is_quality_voted == 'true' ? 'text-success' : 'text-dark'}">
                                <span class="arrow " id="is_quality_voted"><i class="fa fa-caret-up"></i></span>
                                <span class="count">${courseDetails.quality}</span>
                                </a>
                                <p class="mt-2 ml-3">Content quality</p>
                            </div>
                            </div>
                    <div class="one-liner w-25 mr-3">
                        <div class="tut-vote info d-flex">
                            <a href="javascript:void(0);" id="course_pace" class="d-flex desc flex-column ${courseDetails.is_course_pace_voted == 'true' ? 'text-success' : 'text-dark'}">
                            <span class="arrow " id="is_course_pace_voted"><i class="fa fa-caret-up"></i></span>
                            <span class="count">${courseDetails.course_pace}</span>
                            </a>
                            <p class="mt-2 ml-3">Course Pace</p>
                        </div>
                        </div>
                <div class="one-liner w-25 mr-3">
                    <div class="tut-vote info d-flex">
                        <a href="javascript:void(0);" id="quality_votes" class="d-flex desc flex-column ${courseDetails.is_quality_votes_voted == 'true' ? 'text-success' : 'text-dark'}">
                        <span class="arrow" id="is_quality_votes_voted"><i class="fa fa-caret-up"></i></span>
                        <span class="count">${courseDetails.quality_votes}</span>
                        </a>
                        <p class="mt-2 ml-3">Video quality</p>
                    </div>
                    </div>
                    <div class="one-liner w-25 mr-3">
                            <div class="tut-vote info d-flex">
                                <a href="javascript:void(0);" id="depth" class="d-flex desc  flex-column ${courseDetails.is_depth_voted == 'true' ? 'text-success' : 'text-dark'}">
                                <span class="arrow" id="is_depth_voted"><i class="fa fa-caret-up"></i></span>
                                <span class="count">${courseDetails.depth}</span>
                                </a>
                                <p class="mt-2 ml-3">Course depth and Coverage</p>
                            </div>
                            </div>
                <div class="one-liner w-25 mr-3">
                        <div class="tut-vote info d-flex">
                            <a href="javascript:void(0);" id="instructor" class="d-flex desc flex-column ${courseDetails.is_instructor_voted == 'true' ? 'text-success' : 'text-dark'}">
                            <span class="arrow" id="is_instructor_voted"><i class="fa fa-caret-up"></i></span>
                            <span class="count">${courseDetails.instructor}</span>
                            </a>
                            <p class="mt-2 ml-3">Qualified Instructor</p>
                        </div>
                        </div>
                
                                 
                    </div>
            </div>
          </div> 
        </div> 
      
      </div>
        `;
        $('#course-header').append(courseHeader);
    }

      function populateCourseComments(courseComments){
        if(courseComments){
            commentsLength = courseComments.comments.length;
            updateCommentsCount();
            let list = courseComments.comments.map((elem, index) => {
                 return commentTemplate(elem);
              });
              return list.join('');
             }
      }

      function updateCommentsCount(){
          $('#comments-count').empty();
          const countTmpl = `
          Total Comments (${commentsLength})
          `;
          $('#comments-count').append(countTmpl);
      }
     
    function commentTemplate(elem) {
        let time = new Date(elem.time);
        return  `
        <div class="panel panel-white post panel-shadow mb-5 bg-white">
        <div class="post-heading">
            <div class="pull-left image">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="img-circle avatar" alt="user profile image">
            </div>
            <div class="pull-left meta">
                <div class="title h5">
                    <span><b>${elem.username}</b></span>
                    made a post.
                </div>
                <h6 class="text-muted time">${time}</h6>
            </div>
        </div> 
        <div class="post-description"> 
            <p>${elem.comment}</p>
        </div>
    </div>
 `;
    }

    function addComment(){
        isLoggedOut = $('#showsafterlogin').hasClass('d-none');
        if(!isLoggedOut){
            let comment =  $('textarea.postComment').val();
          if(comment != ""){
            let date = new Date();
            let obj = {};
            obj.comment = comment;
            obj.time = date;
            obj.username = 'Sonal';
            storeNewComment(obj);
            updateCommentsView(obj);
          }
        } else {
            $('#signInModal').modal('show');
         }
     }

    function storeNewComment(comment){
         commentsLength = +commentsLength + 1;
        firebaseMethods.updateUserComments(commentsLength,comment,course_id);
    }

    function updateCommentsView(comment){
        updateCommentsCount();
        let commentSection = commentTemplate(comment);
        $('#commentSection').append(commentSection);
        $('#comment').val('');
    }

    function initializeVoteEventListener(){
        $("a.vote-widget").click('on',function(e) {
        isLoggedOut = $('#showsafterlogin').hasClass('d-none');
        if(!isLoggedOut){
            e.preventDefault();
            let isVoted = $(e.target).hasClass('bg-success') ? "false" : "true";
            let upvote_count = upVoteCourse.vote(e);
            firebaseMethods.addVoteToCourse(course_id,upvote_count,isVoted); 
        } else {
            $('#signInModal').modal('show');
         }
           });
        }
       

    return {
        init: init,
        addComment:addComment
    }

})();