var upVoteCourse = (function(){
    return {
       vote : function(e){
            $(e.target).toggleClass('bg-success text-white');
            var course_id = $(e.target).closest('.card')[0].id;
            var upvote_count =  parseInt($(e.target).find('.count').text(),10);
            $(e.target).hasClass('bg-success') ? upvote_count += 1 : upvote_count -= 1;
            $(e.target).find('.count').text(upvote_count);
            return upvote_count;
       }
    }
 })();