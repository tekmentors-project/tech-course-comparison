// Initialize Firebase
  var config = {
    apiKey: "AIzaSyDuDJTmUOqFfXz8Q-1wcH3vR20hSQaPJhY",
    authDomain: "ilearn360-6ca2b.firebaseapp.com",
    databaseURL: "https://ilearn360-6ca2b.firebaseio.com",
    projectId: "ilearn360-6ca2b",
    storageBucket: "ilearn360-6ca2b.appspot.com",
    messagingSenderId: "571934873810"
  };
  firebase.initializeApp(config);


const auth = firebase.auth();
const rootref = firebase.database().ref();
const txtEmail = document.getElementById('txtEmail');
const txtPassword = document.getElementById('txtPassword');
const btnLogin = document.getElementById('btnLogin');
const btnSignup = document.getElementById('btnSignup');
const btnLogout = document.getElementById('btnLogout');


//add login event 
btnLogin.addEventListener('click', e=>{
const email = txtEmail.value;
const pass = txtPassword.value;

const promise = auth.signInWithEmailAndPassword(email, pass);
promise.catch(e =>console.log(e.message));
});

//add a sign up event
btnSignup.addEventListener('click', e=>{
    const email = txtEmail.value;
    const pass = txtPassword.value;
    const promise = auth.createUserWithEmailAndPassword(email, pass);
    promise.catch(e =>console.log(e.message));
    });
    btnLogout.addEventListener('click', e => {
    firebase.auth().signOut();
});
    // add realime listner
firebase.auth().onAuthStateChanged(firebaseUser =>{
if(firebaseUser){
    console.log(firebaseUser);
    btnLogout.classList.remove('hide');
    btnLogin.classList.add('hide');
    btnSignup.classList.add('hide');
}else{
    console.log('Not Logged In');  
    btnLogout.classList.add('hide');
    btnLogin.classList.remove('hide');
    btnSignup.classList.remove('hide');
}
    });

    btnLoadData.addEventListener('click', e =>{
    let query = rootref.child("course_details").child('course_01a'); 
    query.once("value").then(function(snapshot) {
            var key = snapshot.key;
              // childData will be the actual contents of the child
              var childData = snapshot.val();
            console.log(childData.author);
         
        });
    });