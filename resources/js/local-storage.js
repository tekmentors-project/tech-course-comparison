var localStorageLayer = (function(){
    return {

        setSelectedCategory: function (category_obj) {
           localStorage.setItem('Category_Data',JSON.stringify(category_obj));
        },

        getCategoryData: function (category) {
            const templateid = localStorage.getItem(category);
            if (templateid) return templateid;
            else return null;
        }

    }
})();