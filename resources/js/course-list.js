$(document).ready(function(){
  //  coursesList.init();
 });

 var coursesList = (function(){
    var isLoggedOut = true;
    function init(){
       
        initializeFilterSection();
        initializeCoursesList();
    }


    function initializeFilterSection(){
        filterCourses.filter();
    }

    function initializeCoursesList(){
        let category_data = JSON.parse(localStorageLayer.getCategoryData('Category_Data'));
        updateCourseHeader(category_data);
        fetchCoursesList(category_data.id).then(function() {  
            $('#spinner').removeClass('d-flex').addClass('d-none');
            initializeVoteEventListener(); 
            initializeSortEventListener(); 
         });
    }

    function updateCourseHeader(category_data){
        const c_header = `
        ${category_data.name} Tutorials and Courses
      `;
      $('.tut-title').append(c_header);
      const c_subheader = `
          Learn ${category_data.name} online from the best ${category_data.name} tutorials submitted & voted by the programming community.
      `;
      $('.page--section-subheading').append(c_subheader);
      $('#course-img').attr('src', category_data.image);
 
    }
    
    function initializeSortEventListener(){
        $('#sort a').unbind('click').click('on',function(e) {
            $('#sort a').removeClass('text-success font-weight-bold');
             var $sorted_items;
             if($(this)[0].id == 'vote') {
               $sorted_items =  sortMethods.getVoteSorted('#courseList .card', 'data-vote');
               sortMethods.toggleSortArrow('vote');
             } else if($(this)[0].id == 'submit') {
               $sorted_items =  sortMethods.getAuthorSorted('#courseList .card', 'data-submit'); 
               sortMethods.toggleSortArrow('submit');
             }
            
             $('#courseList').empty();
             $('#courseList').append($sorted_items);
        });
    }

   

   
    
    function initializeVoteEventListener(){
    $("a.vote-widget").click('on',function(e) {
        isLoggedOut = $('#showsafterlogin').hasClass('d-none');
        if(!isLoggedOut){
            e.preventDefault();
            let course_id = $(this).closest('.card')[0].id;
            let isVoted = $(e.target).hasClass('bg-success') ? "false" : "true";
            let upvote_count = upVoteCourse.vote(e);
            firebaseMethods.addVoteToCourse(course_id,upvote_count,isVoted); 
        } else {
           $('#signInModal').modal('show');
        }
       });
    }

    function fetchCoursesList(category){
        $('#spinner').addClass('d-flex').removeClass('d-none');
       return firebaseMethods.fetchSelectedCategoryCourses(category).then((courses) => {
            courses.forEach(function (course) {
                var key = course.key;
                var childData = course.val();
                let hostName = common.queryCourseDomain(childData.course_url);
                populateCoursesData(childData,key,hostName);
                });
       });
 }
    
 function populateCoursesData(course,catogery_id,hostName){
    const tmpl = `
    <div class="card mt-3" id=${catogery_id} data-type=${course.type} data-medium=${course.medium} data-level=${course.level} data-vote=${course.upvote} data-submit=${course.submittedBy}>
    <div class="card-header d-flex" id="headingOne">
          <div class="tut-vote mr-3 ">
               <a href="javascript:void(0)" class="vote-widget ${course.isVoted == 'true' ? 'bg-success text-white' : ''}" data-topic=${course.title} >
                      <span class="arrow">
                          <i class="fa fa-caret-up"></i>
                      </span>
                      <span class="count">${course.upvote}</span>
                  </a>
           </div>
          <div class="tut-title">
              <div class="title-links mb-2">
                  <a onclick="coursesList.renderDetailsPage('${catogery_id}','${course.title}');">
                  <span class="tutorial-title-txt">${course.title}</span>
                  <span class="tut-title-link">
                      <small class="js-tutorial text-dark" data-id="3768" title=${course.title} target="_blank">(${hostName})</small>
                  </span>
               </a>
          </div>
          <div class="tut-footer">
                  <span class="badge badge-info">${course.type}</span>
                  <span class="badge  badge-info">${course.medium}</span>
                  <span class="badge badge-info">${course.level}</span>
                  <small class="ml-3">Submitted by <strong>${course.submittedBy}</strong></small>
                  <a><i class="fa fa-bookmark ml-3 text-secondary"></i><small> Bookmark</small></a>
          </div>
      </div>
    

    </div>
    </div>
     `;
     $('#courseList').append(tmpl);
}

    function renderDetailsPage(catogery_id,title){
        $('#render-content').empty();
        initializeDetailsTemplate(catogery_id,title);
    }

    function initializeDetailsTemplate(catogery_id,title){

        const courseDetailsTmpl = `
        <div class="container mb-5" id="courseDetails">
        <article>
            <section class="mt-5">
                <h3 class="tut-title"></h3> 
                <div class="tut-description hidden-xs">
                    <p class="page--section-subheading mb-10">
                    </p>
                 </div>
            </section>
            <div class="row mt-5">
            <div class="col-md-9 mt-5">
                <div id="spinner" class="lds-spinner d-none"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        
                        
                    <div id="course-header"></div>

                          <div class="accordion mt-5" id="">
                            
                                  <div class="card">
                                  <div class="card-header d-flex" id="details-section">
                                        <div class="tut-title">
                                           <h3>Course Details</h3>
                                    </div>
                                  </div>
                              
                               <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                            <div class="section-title text-left">
                                             <p class="sec--title font-weight-bold">What you'll learn </p>
                                            </div>   
                                            <div class="info-section-container d-flex flex-wrap">
                                            
                                                    <ul>
                                                    <li>Make REAL web applications using cutting-edge technologies </li>
                                                    <li>Continue to learn and grow as a developer, long after the course ends </li>
                                                    <li> Create a blog application from scratch using Express, MongoDB, and Semantic UI </li>
                                                            <li>Create a complicated yelp-like application from scratch</li>
                                                                    <li> Write your own browser-based game</li>
                                                                            <li>   Create static HTML and CSS portfolio sites and landing pages</li>
                                                                                    <li>     Think like a developer. Become an expert at Googling code questions! </li>
                                                                                            <li>   Create complex HTML forms with validations</li>
                                                                                                    <li>     Write web apps with full authentication</li>
                                                    </ul>
                                                         
                                            </div>
                                    </div>
                                  </div> 
                                </div> 
                              
                              </div>

                              <div class="accordion mt-5 mb-5" id="user_comment">
                                      <div class="card">
                                      <div class="card-header d-flex" id="details-section">
                                            <div class="tut-title">
                                               <h3>Discuss this tutorial</h3>
                                        </div>
                                      </div>
                                  
                                   <div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                                                <div class="section-title text-left">
                                                 <p class="sec--title font-weight-bold">Ask a question or write your feedback/review of this course or tell anything to the people taking this course. </p>
                                                </div>  
                                                <div id="commentSection">
                                                <h5 id="comments-count"></h5>
                                                    </div> 
                                                <div class="info-section-container">
                                                        <textarea class="postComment w-75" rows='2' cols='50' placeholder='Add Comment...' id='comment'></textarea>
                                                        <button class='btn btn-primary align-top ml-3' onClick='courseDetail.addComment(this)'>Post Comment</button></div>
                                                </div>
                                        </div>
                                      </div> 
                                    </div> 
                                  
                                  
              </div>
              <aside class="col-md-3 border border-info mt-5" id="course-aside">
              </aside>
            </div>
        </article>
    </div>
        `;
        $('#render-content').append(courseDetailsTmpl);
        window.history.pushState({}, '', "/course-details/"+title);
        courseDetail.init(catogery_id);
    }

    return {
        init: init,
        renderDetailsPage: renderDetailsPage
    }

})();