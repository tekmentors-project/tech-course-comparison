var common = (function(){
   
    function queryCourseDomain(url){
        let hostname = $('<a>').prop('href', url).prop('hostname');
        return hostname;
    }

    return {
        queryCourseDomain: queryCourseDomain
    }

})();