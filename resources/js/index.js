$(document).ready(function(){
    coursesCatogery. initializeBrowserStates(document.location.href);
  //  coursesCatogery.init();
});

var coursesCatogery = (function(){
     var loggenInUserName = '';
     var isLoggedOut = true;
     function init(){
       
        initialize_Course_Categories();
        initailize_Category_Search();
     }
     
     function initializeBrowserStates(){
       updateTemplateOnBrowserBack(document.location.href);
        window.addEventListener('popstate', function(event) {
            event.preventDefault(); 
            updateTemplateOnBrowserBack(document.location.href);
        });
        history.replaceState({
        
        }, document.title, document.location.href);
     }
     
     function updateTemplateOnBrowserBack(location){
        var currentPageName =  location.split('/')[location.split('/').length - 2];
        renderPageTemplate(currentPageName);
     }

     function renderPageTemplate(currentPageName){
        $('#render-content').empty();
        
        switch(currentPageName) {
            case 'courses':
                 renderCourseListContent();
                break;
            case 'categories':
            window.history.pushState({}, '', "/categories");
            initializeIndexContent();
            coursesCatogery.init();
                break;
            case 'course-details':
             coursesList.renderDetailsPage();
            default:
            window.history.pushState({}, '', "/categories");
            initializeIndexContent();
            coursesCatogery.init();
          
        }
     }
     
     function initializeIndexContent(){
         const indexTmpl = `  <div class="container" id="categories-list">
         <div class="row justify-content-center align-items-center mt-5">
         <div class="alert login-alert alert-success mt-5 w-75 d-none" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <strong>Success!</strong> Logged in successfully!
                 </div>
        <div class="alert logout-alert alert-success mt-5 w-75 d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> You are logged out now!
        </div>
         <div class="alert alert-course alert-success mt-5 w-75 d-none" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <strong>Success!</strong> Course is added successfully!
                 </div>
              <h1 class="mt-sm-5 mt-md-2 mb-3 pt-5">Find the Best Programming Courses & Tutorials</h1> 
             <div class="col-9 ">
                 <div class="input-group mb-3 ">

                     <input type="text" id="myInput" class="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" placeholder="Search for the language you want to learn: Python, Javascript,...">
                     <i class="icon ion-ios-search-strong"></i>
                 </div>
             </div>
         </div>
         <div class="row justify-content-center pt-3 mb-5" id="courses">
             <div id="spinner" class="lds-spinner d-none">
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
                 <div></div>
             </div>
         </div>
     </div>`;
     $('#render-content').empty();
     $('#render-content').append(indexTmpl);
     }

     function renderCourseListContent(){
         const list = `<div class="container" id="courses-list">
         <article>
             <section class="mt-5 d-flex p-3 course-header">
             <img class="mr-3" id="course-img" height="100px" width="100px">
             <div>
                 <h3 class="tut-title"></h3> 
                 <div class="tut-description hidden-xs">
                     <p class="page--section-subheading mb-10">
                     </p>
                  </div>
                  </div>
             </section>
             <div class="row mt-5">
             <div class="col-md-9">
             <div class="tut-list-heading d-flex p-3">
                <span><strong>Top Tutorials</strong></span>
                <span class="ml-auto" id="sort">
                   <strong>Sort:</strong>
                    <a class="pl-2" id="vote">Upvote <i class="fa fa-arrow-up"></i></a>  
                   | <a class="pl-2" id="submit">Submitted by <i class="fa fa-arrow-up"></i></a>  
                </span>
             </div>
                         
                     <div class="accordion mb-5 pb-5" id="courseList">
                     <div class="p-5 m-5 border bg-info text-white d-none" id="no-course-msg">Sorry !! no courses available on the selected filter.<br/>Please try other filters!</div>
                             <div id="spinner" class="lds-spinner d-flex mx-auto d-none"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                      </div>
               </div>
               <aside class="col-md-3 border border-info mb-5">
                     <div class="panel-heading border-bottom py-3">
                          <h5>CHOOSE YOUR COURSE</h5>
                      </div>
                     <div class="filter-section">
                         <div class="mt-3">
                             <h5><strong>Type of course</strong></h5>
                             <form class="mt-3">
                             <div class="checkbox">
                             <label><input type="checkbox" class="type" id="type1" value="Free"> Free
                             <span class="checkmark"></span>
                             </label>
                             </div>
                             <div class="checkbox">
                             <label><input type="checkbox" class="type" id="type2" value="Paid"> Paid
                             <span class="checkmark"></span>
                             </label>
                             </div>
                             </form>
                         </div>
                         <div class="mt-3">
                                 <h5><strong>Medium</strong></h5>
                                 <form class="mt-3">
                                 <div class="checkbox">
                                 <label><input type="checkbox" class="medium" id="medium1" value="Video"> Video
                                 <span class="checkmark"></span>
                                 </label>
                                 </div>
                                 <div class="checkbox">
                                 <label><input type="checkbox" class="medium" id="medium2" value="Book"> Book
                                 <span class="checkmark"></span>
                                 </label>
                                 </div>
                                 </form>
                             </div>
                             <div class="mt-3">
                                 <h5><strong>Level</strong></h5>
                                 <form class="mt-3">
                                 <div class="checkbox">
                                 <label><input type="checkbox" class="level" id="level1" value="Beginner"> Beginner
                                 <span class="checkmark"></span>
                                 </label>
                                 </div>
                                 <div class="checkbox">
                                 <label><input type="checkbox" class="level" id="level2" value="Advanced"> Advanced
                                 <span class="checkmark"></span>
                                 </label>
                                 </div>
                                 </form>
                             </div>
                     </div>
                         
                    
               </aside>
             </div>
         </article>
     </div>`;
     $('#render-content').append(list);
     coursesList.init();
     }

     function initialize_Course_Categories() {
        addCategoriesEventListeners();
        updateCategoriesData();
     }
    
     function addCategoriesEventListeners(){
       
        $(window).on('show.bs.modal', function() { 
            let username =  $('#d-name').text().split(' ')[1];
            model.courseDetails['submittedBy'] = username; 
            let isLoggedOut = $('#showsafterlogin').hasClass('d-none');
            if(!isLoggedOut){
            $('#newCourseModal').modal('show');
            } else {
          //  alert('shown');
            }
        });
        $(document).off('click').on('click', '#categories-list .card', function () {
            let category_obj = {
                'id': $(this)[0].id,
                'name': $(this).find("span.card-text").text(),
                'image': $(this).find("img").attr('src')
            }
            localStorageLayer.setSelectedCategory(category_obj);
            $('#render-content').empty();
            window.history.pushState({}, '', "/courses/"+category_obj.name);
            renderCourseListContent();
        });
     }

     function updateCategoriesData(){
        $('#spinner').removeClass('d-none');
        firebaseMethods.fetchCategoriesData().then((courses) => {
           courses.forEach(function (course) {
            var key = course.key;
            // childData will be the actual contents of the child
            var childData = course.val();
            populateCategories(childData,key);
            populateCategoriesDropdown(childData.name,key); 
            });
        });
     }

     function initailize_Category_Search() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#courses .card").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    }

     function populateCategories(course,catogery_id){
         const tmpl = `
             <div class="card col-sm-12  col-md-3 border border-light m-3" id=${catogery_id}>
                <div class="card-body text-center d-flex">
                   <img class="mr-3" height="50px" width="50px" src=${course.img_url}>
                  <span class="card-text text-dark text-left pt-2">${course.name}</span>
                </div>
              </div>
          `;
          $('#courses').append(tmpl);
          $('#spinner').addClass('d-none');
     }

     function populateCategoriesDropdown(categoriesName,key) {
        var option = $('<option data-id="'+key+'" value="' + categoriesName + '"></option>');
        $('#category-list').append(option);
     }

   
    
    function onNewCourseSubmit() {
        var db_Child = 'courses_list';
       let username =  $('#d-name').text().split(' ')[1];
       model.courseDetails['submittedBy'] = username;
       let isAnyEmpty = checkCourseProperties(model.courseDetails);
      if(isAnyEmpty){
          $('.err-new-course').removeClass('d-none');
          return false;
      }else{
        firebaseMethods.addNewCourseToFirebase(db_Child,model.courseDetails,model.courseAdditinalInfo,model.courseComments)
            .then(() => { 
                $('#newCourseModal').modal('toggle');
                $(".alert.alert-course").removeClass('d-none');
                $('.err-new-course').addClass('d-none');

                window.setTimeout(function() {
                    $(".alert.alert-course").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 4000);
            })
        return false;
        }
    }
    
    function getEventSource(event) {
        if (event.srcElement) return event.srcElement;
        else if (event.target) return event.target;
    }
    
    
    function updateNewCourseModel() {
        let srcElement = getEventSource(event);
        if (srcElement) {
            if(srcElement.name == 'author'){
                model.courseAdditinalInfo[srcElement.name] = srcElement.value;
            } else {
             model.courseDetails[srcElement.name] = srcElement.value;
             srcElement.name == 'course_url' ?  (model.courseAdditinalInfo[srcElement.name] = srcElement.value) : '';
            }
            if(srcElement.name == 'title'){
               var id = $('#category-list option[value="' + srcElement.value +'"]').attr('data-id');
               model.courseDetails['category_id'] = id;
               model.courseAdditinalInfo['category_id'] = id;
            }
        }
    }

    function showModel(){
        let username =  $('#d-name').text().split(' ')[1];
        model.courseDetails['submittedBy'] = username; 
        isLoggedOut = $('#showsafterlogin').hasClass('d-none');
        if(!isLoggedOut){
        $('#newCourseModal').modal('show');
            setTimeout(() => {$("#formElement")[0].reset();}, 200);
        } else {
            $('#signInModal').modal('show');
        }
    }

    function checkCourseProperties(obj) {
        for (var key in obj) {
            if (obj[key] == null || obj[key] == "")
                return true;
        }
        return false;
    }
    
        return {
            init: init,
            updateNewCourseModel: updateNewCourseModel,
            onNewCourseSubmit: onNewCourseSubmit,
            initializeBrowserStates: initializeBrowserStates,
            renderPageTemplate: renderPageTemplate,
            loggenInUserName: loggenInUserName,
            showModel:showModel,
            updateTemplateOnBrowserBack: updateTemplateOnBrowserBack
        }
    
    })();