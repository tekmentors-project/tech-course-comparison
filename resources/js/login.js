const auth = firebase.auth();
const rootref = firebase.database().ref();
const txtEmail = document.getElementById('txtEmail');
const txtPassword = document.getElementById('txtPassword');
const txtSnpEmail = document.getElementById('txtSnpEmail');
const txtSnpPassword = document.getElementById('txtSnpPassword');
const btnLogin = document.getElementById('btnLogin');
const btnSnpSignup = document.getElementById('btnSnpSignup');
const showsafterlogin = document.getElementById('showsafterlogin');
const showsabeforelogin = document.getElementById('showsabeforelogin');
const username = document.getElementById('txtUserName');
const dname = document.getElementById('d-name');
// const btnSignup = document.getElementById('btnSignup');
const btnLogout = document.getElementById('btnLogout');
var updateUserProfile = false;
 
function toggleDropDown(){
    $('#signupDropdown').toggleClass('show');
    $('#signinDropdown').removeClass('show');
}

function toggleLinks(){
    $('#savedActions').toggleClass('show');
}

function toggleSignIn(){
    $('#signinDropdown').toggleClass('show');
    $('#signupDropdown').removeClass('show');
}

//add login event 
btnLogin.addEventListener('click', e =>{
const email = txtEmail.value;
const pass = txtPassword.value;
    $('#signupDropdown').removeClass('show');
const promise = auth.signInWithEmailAndPassword(email, pass);
promise.catch(e => $('.error-signin').empty().append(e.message));
});

//add a sign up event
btnSnpSignup.addEventListener('click', e=>{
    const email = txtSnpEmail.value;
    const pass = txtSnpPassword.value;
    updateUserProfile = true;
    $('#signinDropdown').removeClass('show');
    const promise = auth.createUserWithEmailAndPassword(email, pass);
    promise.catch(e => $('.error-signup').empty().append(e.message));
    });

    btnLogout.addEventListener('click', e => {
    $('#signinDropdown').removeClass('show');
    $('#signupDropdown').removeClass('show');
   
    $(".alert.logout-alert").removeClass('d-none');
    $(".alert.logout-alert").empty().append('You are logged out now!');
    window.setTimeout(function() {
        $(".alert.logout-alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 4000);
    firebase.auth().signOut();
});
    // add realime listner
firebase.auth().onAuthStateChanged(firebaseUser =>{
if(firebaseUser){
    if(updateUserProfile){
     writeUserProfile(firebaseUser.uid, username.value);
     getUserProfile(firebaseUser.uid).then(function(result){
        dname.innerHTML = "Welcome " + result;
     });
     
    }else {
    getUserProfile(firebaseUser.uid).then(function(result){
        dname.innerHTML = "Welcome " +  result;
        coursesCatogery.loggenInUserName = result;
     });;
}
    showsafterlogin.classList.remove('d-none');
    showsabeforelogin.classList.add('d-none');
    
}else{
    showsafterlogin.classList.add('d-none');
    showsabeforelogin.classList.remove('d-none');
    
}
    });

    function writeUserProfile(userId, username) {
        console.log("writeUserProfile called");
    firebase.database().ref('usersProfile/' + userId).set({
          username: username,
        });
         
      }  

      function getUserProfile(userId) {
        console.log("getUserProfile called");
        let query = rootref.child("usersProfile").child(userId); 
        return query.once("value").then(function(snapshot) {
            var usernamedb = snapshot.val();
            console.log("getUserProfile called" + usernamedb.username);
            $(".alert.login-alert").removeClass('d-none');
                window.setTimeout(function() {
                    $(".alert.login-alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).addClass('d-none'); 
                    });
                }, 4000);
            return usernamedb.username; 
        });
      } 