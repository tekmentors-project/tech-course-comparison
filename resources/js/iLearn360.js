$(document).ready(function(){
    coursesCatogery.init();
});

var coursesCatogery = (function(){
     function init(){
        $(document).on('click', '.card', function () {
            let category_obj = {
                'id': $(this)[0].id,
                'name': $(this).find("span.card-text").text()
            }
            localStorageLayer.setSelectedCategory(category_obj);
            var url = "./course-list.html";
            window.open(url,"_self");
        });
        
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#courses .card").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        fetchCourses();
     }

     function fetchCourses(){
            $('#spinner').removeClass('d-none');
            let query = firebase.database().ref("categories").orderByKey();
            query.once("value")
              .then(function(snapshot) {
              snapshot.forEach(function(childSnapshot) {
                  var key = childSnapshot.key;
                  // childData will be the actual contents of the child
                  var childData = childSnapshot.val();
                  populateCategories(childData,key);
                  populateCategoriesDropdown(childData.name,key); 
              });
           });
     }

     function populateCategories(course,catogery_id){
         const tmpl = `
             <div class="card  col-3 border border-light m-3" id=${catogery_id}>
                <div class="card-body text-center d-flex">
                   <img class="mr-3" height="50px" width="50px" src=${course.img_url}>
                  <span class="card-text text-dark text-left pt-2">${course.name}</span>
                </div>
              </div>
          `;
          $('#courses').append(tmpl);
          $('#spinner').addClass('d-none');
     }

     function populateCategoriesDropdown(categoriesName,key) {
        var option = $('<option data-id="'+key+'" value="' + categoriesName + '"></option>');
        $('#category-list').append(option);
     }

     var courseDetails = {
        course_url: '',
        category_id:'',
        level:'',
        medium:'',
        submittedBy:'Sonal',
        title:'',
        type:'',
        upvote:'0'
    }
    
    function onSubmit() {
        var db_Child = 'courses_list';
        writeDataToFirebase(db_Child,courseDetails)
            .then(() => { 
                $('#myModal').modal('toggle');
                $(".alert").removeClass('d-none')
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 4000);
            })
        return false;
    }
    
    function getEventSource(event) {
        if (event.srcElement) return event.srcElement;
        else if (event.target) return event.target;
    }
    
    
    function updateModel() {
        let srcElement = getEventSource(event);
        if (srcElement) {
            courseDetails[srcElement.name] = srcElement.value;
            if(srcElement.name == 'title'){
               var id = $('#category-list option[value="' + srcElement.value +'"]').attr('data-id');
                courseDetails['category_id'] = id;
            }
            // const validationResult = validations[srcElement.name](srcElement.value);
            // if (!validationResult.result) {
            //     errorMsg[srcElement.name] = validationResult.msg;
            // } else {
            //     errorMsg[srcElement.name] = '';
            // }
        }
    }
    
    function writeDataToFirebase(db_Child, postData) {
        // Get a key for a new Post.
        var newPostKey = firebase.database().ref().child(db_Child).push().key;
      
        // Write the new post's data simultaneously in the posts list and the user's post list.
        var updates = {};
        updates['/'+db_Child+'/' + newPostKey] = postData;
      
        return firebase.database().ref().update(updates);
    }
    
    

        return {
            init: init,
            updateModel: updateModel,
            onSubmit: onSubmit
        }
    
    })();